// Our data object
var data = { a: 1 }
//-- Stop Reactivity
// which prevents existing properties from being changed, which also means the reactivity system can’t track changes
//Object.freeze(data);
var vm = new Vue({
  	data: data
})

//defined properties $
//From Console
if (vm.$data === data) window.console.log ("vm.$data === data is True");
//props, options, parent (Vue Instance), root (Vue Instance), children (Array<Vue instance>), slots ({ [name: string]: ?Array<VNode>}),
//scopedSlots ({ [name: string]: props => VNode | Array<VNode> }), refs (Object)...

// ----  Reactivity -----
// Setting the property on the instance
// also affects the original data
//From Console
vm.a = 2;
window.console.log("data.a is ", data.a);

// ... and vice-versa
//From Console
data.a = 3;
window.console.log("vm.a is ", vm.a);


