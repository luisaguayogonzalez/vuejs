Vue.component('todo-list', {
  // The todo-item component now accepts a
  // "prop", which is like a custom attribute.
  // This prop is called todo.
  props: ['todo'],
  template: '<li>{{ todo.TEXT + " " + todo.PRICE}}</li>'
})

var app = new Vue({
	  el: '#app',
    data: {
	    message: 'Hello Luis. This is your first vue app!',
      rawHtml: '<span style="color:red">This should be red</span>',
      list: [
      { ID: 0, NAME: "Lettuce", TEXT: 'Vegetables', PRICE: "22.4"},
      { ID: 1, NAME: "dairy products", TEXT: 'Cheese', PRICE: "1.4" },
      { ID: 2, NAME: "meat", TEXT: 'Steak beef', PRICE: "18.4" }
      ],
      counter: 1,
      url: "http://www.as.com",
      seen: false
    },
    methods: {
        doSomething: function () {
          this.seen = (this.seen)? false : true;
        }
      },
    computed: {
      // a computed getter
      reversedMessage: function () {
        // `this` points to the vm instance
        return this.message.split('').reverse().join('')
      },
      now: function () {
        return Date.now()
      },
      messageComputed:{ //It should be diferent name witch any data name
        //getter
        get : function(){
          return this.message
        },
        //setter
        set: function(newMessage){
          this.message = newMessage
        }
      }
    }
	})

var app2 = new Vue({
  el: '#app-2',
  data: {
    message: 'You loaded this page on ' + new Date().toLocaleString()
  }
})

var app3 = new Vue({
  el: '#app-3',
  data: {
    seen: false
  }
})

var app4 = new Vue({
  el: '#app-4',
  data: {
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue' },
      { text: 'Build something awesome' }
    ]
  }
})

var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Hello Vue.js!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})

var app6 = new Vue({
  el: '#app-6',
  data: {
    message: 'Hello Vue!'
  }
})